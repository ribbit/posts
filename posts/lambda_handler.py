from os import environ

import awsgi

from posts.v1.v1_api import create_api
from posts.subribbits_client import HttpSubribbitsClient


SUBRIBBITS_CLIENT = HttpSubribbitsClient(environ["SUBRIBBITS_HOST"])
APP = create_api(SUBRIBBITS_CLIENT)


def handle_apigw(event, context):
    return awsgi.response(APP, event, context)  # pragma: no cover
