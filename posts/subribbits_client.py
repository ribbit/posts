from typing import Set

from requests import Session


class SubribbitsClient(object):
    def filter_allowed_subribbits(self, auth_header: str, names: Set[str]) -> Set[str]:
        raise NotImplementedError()  # pragma: no cover

    def is_allowed(self, auth_header: str, name: str) -> bool:
        return name in self.filter_allowed_subribbits(auth_header, {name})


class HttpSubribbitsClient(SubribbitsClient):

    def __init__(self, host: str, session: Session = None):
        self.host = host
        self.session = session or Session()

    def filter_allowed_subribbits(self, auth_header: str, names: Set[str]) -> Set[str]:
        response = self.session.get(
            f"{self.host}/subribbits/v1/r",
            params=dict(name=",".join(names)),
            headers=dict(Authorization=auth_header)
        )
        response.raise_for_status()

        subribbits = response.json()
        return {subribbit["name"] for subribbit in subribbits}
