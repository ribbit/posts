from os import environ
from secrets import token_urlsafe
from datetime import datetime
from enum import Enum
from typing import Type

from pynamodb.models import Model
from pynamodb.constants import STRING
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute
from pynamodb.indexes import LocalSecondaryIndex, AllProjection


# TODO make a new library for this
class EnumUnicodeAttribute(UnicodeAttribute):
    """
    An enumerated unicode attribute
    """

    attr_type = STRING

    def __init__(self, enum_type: Type[Enum], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.enum_type: Type[Enum] = enum_type

    def serialize(self, value):
        """ Raises ValueError if input value not in ENUM, otherwise continues as parent class """
        if value not in self.enum_type:
            raise ValueError(
                f"{self.attr_name} must be one of {self.enum_type}, not '{value}'"
            )  # pragma: no cover
        else:
            return UnicodeAttribute.serialize(self, value.name)

    def deserialize(self, value) -> Type[Enum]:
        return self.enum_type[value]


class PostType(Enum):
    text = 1
    image = 2
    youtube = 3
    link = 4
    video = 5


class SearchIndex(LocalSecondaryIndex):
    class Meta:
        index_name = "search"
        projection = AllProjection()

    subribbit = UnicodeAttribute(hash_key=True)
    posted = UTCDateTimeAttribute(range_key=True)


class Post(Model):
    class Meta:
        table_name = environ["POSTS_TABLE_NAME"]
    search_index = SearchIndex()

    subribbit = UnicodeAttribute(hash_key=True)
    id = UnicodeAttribute(range_key=True, default=lambda: token_urlsafe(12))
    posted = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
    title = UnicodeAttribute(null=False)
    type: PostType = EnumUnicodeAttribute(PostType, null=False)
    value = UnicodeAttribute(null=False)
    author = UnicodeAttribute(null=True)
