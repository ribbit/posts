from marshmallow import Schema, fields, ValidationError
from marshmallow_enum import EnumField
import falcon
from falcon import Request, Response, API

from .post import Post, PostType
from ..subribbits_client import SubribbitsClient


class PostSchema(Schema):
    subribbit = fields.String(dump_only=True)
    id = fields.UUID(dump_only=True)
    posted = fields.DateTime(dump_only=True, format="iso8601")
    title = fields.String(required=True)
    type = EnumField(PostType, required=True)
    value = fields.String(required=True)
    author = fields.String(dump_only=True)


SCHEMA = PostSchema()


class PostsResource(object):

    def __init__(self, subribbits_client: SubribbitsClient):
        self.subribbits_client = subribbits_client

    def on_post(self, request: Request, response: Response, subribbit: str):
        try:
            args: dict = SCHEMA.load(request.media)
        except ValidationError as e:
            raise falcon.HTTPBadRequest(str(e))
        author = request.context.user_id

        if not self.subribbits_client.is_allowed(request.context.auth_header, subribbit):
            raise falcon.HTTPForbidden()

        post = Post(
            subribbit=subribbit,
            title=args["title"],
            type=args["type"],
            value=args["value"],
            author=author,
        )
        post.save()

        response.media = SCHEMA.dump(post)

    def on_get(self, request: Request, response: Response, subribbit: str):
        if subribbit == 'all':
            posts = list(Post.scan())  # TODO find a better way to do this.  Maybe this table needs to be SQL
            posts = sorted(posts, key=lambda p: p.posted, reverse=True)
        else:
            posts = list(Post.search_index.query(subribbit, scan_index_forward=False))
        print(f"Found {len(posts)} posts")

        all_subribbits = {post.subribbit for post in posts}
        print(f"Posts are for subs: {all_subribbits}")
        allowed_subribbits = self.subribbits_client.filter_allowed_subribbits(
            request.context.auth_header, all_subribbits
        )
        print(f"Allowed on subs: {allowed_subribbits}")
        filtered_posts = list(filter(lambda p: p.subribbit in allowed_subribbits, posts))
        print(f"Filtered to {len(filtered_posts)} posts")

        response.media = SCHEMA.dump(filtered_posts, many=True)


class PostByIdResource(object):

    def __init__(self, subribbits_client: SubribbitsClient):
        self.subribbits_client = subribbits_client

    @staticmethod
    def on_delete(request: Request, response: Response, subribbit: str, post_id: str):
        try:
            post = Post.get(subribbit, post_id)
            if post.author != request.context.user_id:
                raise falcon.HTTPForbidden()

            post.delete()
            response.media = SCHEMA.dump(post)
        except Post.DoesNotExist:
            raise falcon.HTTPNotFound()


def create_api(subribbits_client: SubribbitsClient) -> API:
    class CORSComponent(object):
        def process_response(self, req, resp, resource, req_succeeded):
            resp.set_header('Access-Control-Allow-Origin', '*')

    class AuthComponent(object):
        def process_request(self, request: Request, response: Response):
            try:
                request.context.auth_header = request.get_header("Authorization")
                request.context.user_id = request.env["awsgi.event"]["requestContext"]["authorizer"]["principalId"]
            except KeyError:
                raise falcon.HTTPUnauthorized()

    app = API(middleware=[CORSComponent(), AuthComponent()])
    app.add_route("/posts/v1/r/{subribbit}/p", PostsResource(subribbits_client))
    app.add_route("/posts/v1/r/{subribbit}/p/{post_id}", PostByIdResource(subribbits_client))
    return app
