from typing import Set

from posts.subribbits_client import SubribbitsClient


class FakeSubribbitsClient(SubribbitsClient):

    def __init__(self):
        self.forbidden: Set[str] = set()

    def filter_allowed_subribbits(self, auth_header: str, names: Set[str]) -> Set[str]:
        filtered = filter(lambda n: n not in self.forbidden, names)
        return set(filtered)
