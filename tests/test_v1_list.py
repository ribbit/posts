from datetime import datetime

import pytest
from falcon.testing import TestClient as FalconClient
from moto import mock_dynamodb2

from posts.v1.v1_api import create_api
from posts.v1.post import Post, PostType
from tests.fake_subribbits_client import FakeSubribbitsClient


@pytest.fixture(name="client")
def client_fixture(subribbits_client: FakeSubribbitsClient) -> FalconClient:
    dynamo = mock_dynamodb2()
    dynamo.start()
    Post.create_table(read_capacity_units=1, write_capacity_units=1)
    api = create_api(subribbits_client)
    yield FalconClient(api)
    dynamo.stop()


@pytest.fixture(name="post1")
def post1_fixture() -> Post:
    post = Post(
        id='id1',
        subribbit="tests",
        title="Test 1",
        type=PostType.text,
        value="Test content",
        author="user123",
        posted=datetime(2018, 2, 1),
    )
    post.save()
    return post


@pytest.fixture(name="post2")
def post2_fixture() -> Post:
    post = Post(
        id='id2',
        subribbit="things",
        title="Thing 1",
        type=PostType.text,
        value="Thing Content",
        author="user456",
        posted=datetime(2018, 2, 2),
    )
    post.save()
    return post


@pytest.fixture(name='subribbits_client')
def subribbits_client_fixture() -> FakeSubribbitsClient:
    return FakeSubribbitsClient()


def user_env(user_id: str) -> dict:
    return {"awsgi.event": {"requestContext": {"authorizer": {"principalId": user_id}}}}


def test_list_empty(client: FalconClient):
    response = client.simulate_get(
        "/posts/v1/r/tests/p",
        extras=user_env("user123"),
    )
    assert response.status_code == 200
    assert response.json == []


def test_list_specific_subribbit(client: FalconClient, post1: Post, post2: Post):
    response = client.simulate_get(
        f"/posts/v1/r/{post1.subribbit}/p",
        extras=user_env("user123"),
    )
    assert response.status_code == 200

    assert response.json == [
        {
            'subribbit': 'tests', 'author': 'user123', 'type': 'text', 'value': 'Test content',
            'id': 'id1', 'title': 'Test 1', 'posted': '2018-02-01T00:00:00+00:00'
        }
    ]


def test_list_specific_subribbit_forbidden(
        client: FalconClient, post1: Post, subribbits_client: FakeSubribbitsClient
):
    subribbits_client.forbidden.add(post1.subribbit)

    response = client.simulate_get(
        f"/posts/v1/r/{post1.subribbit}/p",
        extras=user_env("user123"),
    )
    assert response.status_code == 200

    assert response.json == []


def test_list_all(
        client: FalconClient, post1: Post, post2: Post, subribbits_client: FakeSubribbitsClient
):
    response = client.simulate_get(
        "/posts/v1/r/all/p",
        extras=user_env("user123"),
    )
    assert response.status_code == 200

    assert response.json == [
        {
            'subribbit': 'things', 'author': 'user456', 'type': 'text', 'value': 'Thing Content',
            'id': 'id2', 'title': 'Thing 1', 'posted': '2018-02-02T00:00:00+00:00'
        },
        {
            'subribbit': 'tests', 'author': 'user123', 'type': 'text', 'value': 'Test content',
            'id': 'id1', 'title': 'Test 1', 'posted': '2018-02-01T00:00:00+00:00'
        }
    ]


def test_list_ordered_for_subribbit(client: FalconClient, post1: Post, post2: Post):
    post1.posted = datetime(2019, 9, 9)
    post1.subribbit = "things"
    post1.save()

    post3 = Post(
        id='id3',
        subribbit="things",
        title="Thing 3",
        type=PostType.text,
        value="Thing3 Content",
        author="user456",
        posted=datetime(2019, 9, 10),
    )
    post3.save()

    post2.posted = datetime(2019, 9, 11)
    post2.subribbit = "things"
    post2.save()

    response = client.simulate_get(
        "/posts/v1/r/things/p",
        extras=user_env("user123"),
    )
    assert response.status_code == 200

    post_ids = [post["id"] for post in response.json]
    assert post_ids == ["id2", "id3", "id1"]
