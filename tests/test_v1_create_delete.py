from datetime import datetime

import pytest
from falcon.testing import TestClient as FalconClient, Result
from moto import mock_dynamodb2

from posts.v1.v1_api import create_api
from posts.v1.post import Post, PostType
from tests.fake_subribbits_client import FakeSubribbitsClient


@pytest.fixture(name="client")
def client_fixture(subribbits_client: FakeSubribbitsClient) -> FalconClient:

    dynamo = mock_dynamodb2()
    dynamo.start()
    Post.create_table(read_capacity_units=1, write_capacity_units=1)
    api = create_api(subribbits_client)
    yield FalconClient(api)
    dynamo.stop()


@pytest.fixture(name="post1")
def post1_fixture() -> Post:
    post = Post(
        id='id1',
        subribbit="tests",
        title="Test 1",
        type=PostType.text,
        value="Test content",
        author="user123",
        posted=datetime(2018, 2, 1),
    )
    post.save()
    return post


@pytest.fixture(name="post2")
def post2_fixture() -> Post:
    post = Post(
        id='id2',
        subribbit="things",
        title="Thing 1",
        type=PostType.text,
        value="Thing Content",
        author="user456",
        posted=datetime(2018, 2, 2),
    )
    post.save()
    return post


@pytest.fixture(name='subribbits_client')
def subribbits_client_fixture() -> FakeSubribbitsClient:
    return FakeSubribbitsClient()


def user_env(user_id: str) -> dict:
    return {"awsgi.event": {"requestContext": {"authorizer": {"principalId": user_id}}}}


def test_create_no_value(client: FalconClient):
    response = client.simulate_post(
        "/posts/v1/r/tests/p",
        json=dict(title="foo", type="text"),
        extras=user_env("user123"),
    )

    assert response.status_code == 400


def test_create_invalid_type(client: FalconClient):
    response = client.simulate_post(
        "/posts/v1/r/tests/p",
        json=dict(title="foo", type="foo", value="my content"),
        extras=user_env("user123"),
    )
    assert response.status_code == 400


def test_create(client: FalconClient):
    response: Result = client.simulate_post(
        "/posts/v1/r/tests/p",
        json=dict(title="foo", type="text", value="my content"),
        extras=user_env("user123"),
    )
    assert response.status_code == 200, response.text

    content = response.json
    assert content["id"]
    assert content["title"] == "foo"
    assert content["type"] == "text"
    assert content["value"] == "my content"
    assert content["author"] == "user123"


def test_create_video(client: FalconClient):
    response: Result = client.simulate_post(
        "/posts/v1/r/tests/p",
        json=dict(title="video", type="video", value="https://video.mp4"),
        extras=user_env("user123"),
    )
    assert response.status_code == 200, response.text

    content = response.json
    assert content["id"]
    assert content["title"] == "video"
    assert content["type"] == "video"
    assert content["value"] == "https://video.mp4"
    assert content["author"] == "user123"


def test_create_without_creator(client: FalconClient):
    response = client.simulate_post(
        "/posts/v1/r/tests/p",
        json=dict(title="foo", type="text", value="my content")
    )
    assert response.status_code == 401


def test_create_forbidden_subribbit(client: FalconClient, subribbits_client: FakeSubribbitsClient):
    subribbits_client.forbidden.add("private")

    response = client.simulate_post(
        "/posts/v1/r/private/p",
        json=dict(title="foo", type="text", value="my content"),
        extras=user_env("user123"),
    )

    assert response.status_code == 403


def test_delete_missing(client: FalconClient):
    response = client.simulate_delete(
        "/posts/v1/r/tests/p/post1",
        extras=user_env("user123"),
    )
    assert response.status_code == 404


def test_delete_as_owner(client: FalconClient, post1: Post):
    response = client.simulate_delete(
        f"/posts/v1/r/{post1.subribbit}/p/{post1.id}",
        extras=user_env(post1.author),
    )
    assert response.status_code == 200

    content = response.json
    assert content["title"] == post1.title


def test_delete_not_owner(client: FalconClient, post1: Post):
    response = client.simulate_delete(
        f"/posts/v1/r/{post1.subribbit}/p/{post1.id}",
        extras=user_env("user456"),
    )
    assert response.status_code == 403
