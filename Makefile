test:
	python -m pytest --cov=posts --cov-report term-missing --cov-fail-under 95 --flake8 tests

package:
	sam build
	sam package --s3-bucket io.andrewohara.pipeline --output-template-file .aws-sam/out.yml

deploy: package
	aws cloudformation deploy --template-file .aws-sam/out.yml --stack-name ribbit-posts-dev --capabilities CAPABILITY_IAM

logs:
	sam logs -n ApiHandler -s ribbit-posts-dev