ribbit-posts
============

Service for storing and retriving posts by subribbit

Resources
---------

- `/r/{subribbit}/p`
  - GET: Get all posts for subribbit
  - POST: Create a post in this subribbit
- `/r/{subribbit}/p/{post}`
  - GET: Gets an individual post by subribbit and post id